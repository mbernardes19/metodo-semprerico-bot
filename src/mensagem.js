module.exports = {
    boas_vindas: "Olá, seja bem-vindo!",
    pedir_forma_pagamento: "Qual foi a sua forma de pagamento?",
    pedir_nome_completo: "Qual é o seu nome completo?",
    pedir_telefone: "Qual é o seu telefone com DDD?",
    pedir_email: "Qual é o seu email?",
    nome_completo: "Confirmando...",
    telefone: "Confirmando...",
    email: "Confirmando...",
    confirmacao_nome_completo: {
        positivo: "Ok!",
        negativo: "Insira seu nome completo novamente",
        erro: "Não entendi",
    },
    confirmacao_telefone: {
        positivo: "Ok!",
        negativo: "Insira seu telefone novamente",
        erro: "Não entendi",
    },
    confirmacao_email: {
        positivo: "Ok!",
        negativo: "Insira seu email novamente",
        erro: "Não entendi",
    },
    verificar_monetizze: "Verificando na Monetizze..."
}
